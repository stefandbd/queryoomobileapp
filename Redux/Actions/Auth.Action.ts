// Login
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

// Register
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

// Facebook Login
export const FACEBOOK_LOGIN_REQUEST = 'FACEBOOK_LOGIN_REQUEST';
export const FACEBOOK_LOGIN_SUCCESS = 'FACEBOOK_LOGIN_SUCCESS';
export const FACEBOOK_LOGIN_ERROR = 'FACEBOOK_LOGIN_ERROR';

// Google Login
export const GOOGLE_LOGIN_REQUEST = 'GOOGLE_LOGIN_REQUEST';
export const GOOGLE_LOGIN_SUCCESS = 'GOOGLE_LOGIN_SUCCESS';
export const GOOGLE_LOGIN_ERROR = 'GOOGLE_LOGIN_ERROR';

// Logout
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_ERROR = 'LOGOUT_ERROR';

// Reset password
export const RESET_PASSWORD_REQUEST = 'RESET_PASSWORD_REQUEST';
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_ERROR = 'RESET_PASSWORD_ERROR';

export const registerRequestEmployee = (
  username: string,
  password: string,
  name: string,
  lastname: string,
  type: string,
) => {
  return {
    type: REGISTER_REQUEST,
    payload: {username, password, name, lastname, type},
  };
};
export const registerRequestEmployer = (
  username: string,
  password: string,
  company: string,
  type: string,
) => {
  return {
    type: REGISTER_REQUEST,
    payload: {username, password, company, type},
  };
};
export const registerSuccess = (data: any) => {
  return {type: REGISTER_SUCCESS, payload: data};
};
export const registerError = (err: any) => {
  return {type: REGISTER_ERROR, payload: {err}};
};

export const loginRequest = (username: string, password: string) => {
  return {
    type: LOGIN_REQUEST,
    payload: {username, password},
  };
};
export const loginSuccess = (data: any) => {
  return {type: LOGIN_SUCCESS, payload: {data}};
};
export const loginError = (err: any) => {
  return {type: LOGIN_ERROR, payload: {err: true}};
};

export const logoutRequest = () => {
  return {
    type: LOGOUT_REQUEST,
    payload: {},
  };
};
export const logoutSuccess = () => {
  return {type: LOGOUT_SUCCESS, payload: {}};
};
export const logoutError = (err: any) => {
  return {type: LOGOUT_ERROR, payload: {err}};
};

export const passResetRequest = (username: string) => {
  return {
    type: LOGOUT_REQUEST,
    payload: {username},
  };
};
export const passResetSuccess = (data: any) => {
  return {type: LOGOUT_SUCCESS, payload: {data}};
};
export const passResetError = (err: any) => {
  return {type: LOGOUT_ERROR, payload: {err}};
};
