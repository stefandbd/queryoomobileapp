export const UPDATE_AVATAR_REQUEST = 'UPDATE_AVATAR_REQUEST';
export const UPDATE_AVATAR_SUCCESS = 'UPDATE_AVATAR_SUCCESS';
export const UPDATE_AVATAR_ERROR = 'UPDATE_AVATAR_ERROR';
export const SET_FILE_URL = 'SET_FILE_URL';


export const updateAvatarRequest = (reference: string, source: string) => {
  return {type: UPDATE_AVATAR_REQUEST, payload: {reference, source}};
};
export const updateAvatarSuccess = (data: object) => {
  return {type: UPDATE_AVATAR_SUCCESS, payload: {data}};
};
export const updateAvatarError = (err: object) => {
  return {type: UPDATE_AVATAR_ERROR, payload: {err}};
};
export const setFileURL = (reference: string, source: string) => {
  return { type: SET_FILE_URL, payload: {reference, source}}
};