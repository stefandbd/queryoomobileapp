import {
  UPDATE_AVATAR_REQUEST,
  UPDATE_AVATAR_SUCCESS,
  UPDATE_AVATAR_ERROR,
  SET_FILE_URL,
} from '../Actions/Profile.Action';
import {Action, State} from '../reducers';

const initialState: State = {fetching: false, data: null, err: null};

export const getProfile = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case UPDATE_AVATAR_REQUEST:
      return {
        fetching: true,
        data: null,
        err: null,
      };
    case UPDATE_AVATAR_SUCCESS:
      return {
        fetching: false,
        data: action.payload.data,
        err: null,
      };
    case UPDATE_AVATAR_ERROR:
      return {
        fetching: false,
        data: null,
        err: action.payload.err,
      };
    case SET_FILE_URL:
      return {
        fetching: false,
        data: action.payload.data,
        err: null,
      };
    default:
      return state;
  }
};
