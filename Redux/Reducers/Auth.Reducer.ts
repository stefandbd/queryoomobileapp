import {
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_ERROR,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_SUCCESS,
} from '../Actions/Auth.Action';
import {Action, State} from '../reducers';

const initialState: State = {
  fetching: false,
  data: {
    moreInfo: {
      lastname: '',
      name: '',
      photoURL: '',
      userType: '',
      password: '',
    },
    user: {
      email: '',
    },
  },
  err: null,
};

export const auth = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case REGISTER_REQUEST:
      return {
        ...initialState,
        fetching: true,
      };
    case REGISTER_SUCCESS:
      return {
        ...initialState,
      };
    case REGISTER_ERROR:
      return {
        ...initialState,
        err: action.payload.err,
      };
    case LOGIN_REQUEST:
      return {
        ...initialState,
        fetching: true,
      };
    case LOGIN_SUCCESS:
      return {
        fetching: false,
        data: action.payload.data,
        err: null,
      };
    case LOGIN_ERROR:
      return {
        fetching: false,
        data: null,
        err: action.payload.err,
      };
    case LOGOUT_REQUEST:
      return {
        ...initialState,
        fetching: true,
      };
    case LOGOUT_SUCCESS:
      return {
        ...initialState,
      };
    case LOGOUT_ERROR:
      return {
        fetching: false,
        data: action.payload.data,
        err: action.payload.err,
      };
    case RESET_PASSWORD_REQUEST:
      return {
        fetching: true,
        data: null,
        err: null,
      };
    case RESET_PASSWORD_SUCCESS:
      return {
        fetching: false,
        data: null,
        err: null,
      };
    case RESET_PASSWORD_ERROR:
      return {
        fetching: false,
        data: null,
        err: action.payload.err,
      };
    default:
      return state;
  }
};
