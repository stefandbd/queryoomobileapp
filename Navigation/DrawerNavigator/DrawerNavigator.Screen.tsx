import React from "react";
import { navigationRef } from '../../App';
import { createDrawerNavigator } from "@react-navigation/drawer";
import {NormalStackNavigator} from '../StackNavigator/NormalStackNavigator';
import { NavigationContainer } from "@react-navigation/native";
import { DrawerContent } from './DrawerContent';

export type DrawerParamList = {};

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <NavigationContainer ref={navigationRef}>
    <Drawer.Navigator screenOptions={{swipeEnabled: false}} drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Dashboard" component={NormalStackNavigator} />
    </Drawer.Navigator>
    </NavigationContainer>
  );
}

export default DrawerNavigator;
