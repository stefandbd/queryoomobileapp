import {StyleSheet} from 'react-native';
import {fontFamily, fontSize} from '../../Utils/const';
import ApplicationStyle from '../../Themes/Application.Style';
import colors from '../../Themes/Colors';

export default StyleSheet.create({
  ...ApplicationStyle,
  textItemMenu: {
    fontSize: fontSize.medium,
    fontFamily: fontFamily.regular,
    color: colors.primary,
    marginLeft: 12,
  },
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
    paddingBottom: 20,
  },
  userInfoContainer: {
    borderBottomColor: '#f4f4f4',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 15,
    paddingBottom: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {},
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  titleContainer: {marginLeft: 15, flexDirection: 'column'},
  editIconContainer: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 30,
    width: 60,
    height: 60,
  },
  editIcon: {
    position: 'absolute',
    top: 40,
    right: 10,
    bottom: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: 10,
    width: 20,
    height: 20,
  },
});
