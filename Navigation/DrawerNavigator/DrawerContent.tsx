import React, {useState} from 'react';
import {View, TouchableHighlight} from 'react-native';
import {Avatar, Title, Caption, Drawer} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {useSelector, useDispatch} from 'react-redux';
import styles from './DrawerNavigator.Style';
import TextAvatar from 'react-native-text-avatar';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import {updateAvatarRequest, setFileURL} from '../../Redux/Actions/Profile.Action';

export function DrawerContent(props) {
  const userInfo = useSelector((state) => state.auth.data.moreInfo);
  const user = useSelector((state) => state.auth.data.user);
  const profile = useSelector((state) => state.getProfile.data);
  const dispatch = useDispatch();
  const fullName = userInfo.name + ' ' + userInfo.lastname;
  const actionUpdateAvatarRequest = (reference: String, source: String) =>
    dispatch(updateAvatarRequest(reference, source));

  const options = {
    title: 'Select Avatar',
    customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const onChangeAvatar = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);

      if (response.didCancel) {
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        const fileName = source.uri.substring(source.uri.lastIndexOf('/') + 1);
        const reference = storage().ref(`/avatars/${user.uid}/${fileName}`);
        actionUpdateAvatarRequest(reference, source);
        setFileURL(reference, source);
      }
    });
  };

  return (
    <View style={{flex: 1}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={styles.userInfoContainer}>
              {profile ? (
                <TouchableHighlight onPress={() => onChangeAvatar()}>
                  <Avatar.Image
                    source={{
                      uri: profile,
                    }}
                    size={60}
                  />
                </TouchableHighlight>
              ) : (
                <TouchableHighlight onPress={() => onChangeAvatar()}>
                  <TextAvatar
                    backgroundColor={'orange'}
                    textColor={'#fff'}
                    size={60}
                    type={'circle'}>
                    {fullName}
                  </TextAvatar>
                </TouchableHighlight>
              )}
              <TouchableHighlight
                style={styles.editIconContainer}
                onPress={() => onChangeAvatar()}>
                <View style={styles.editIcon}>
                  <Icon name="pen" size={12} color="#fff" />
                </View>
              </TouchableHighlight>
              <View style={styles.titleContainer}>
                <Title style={styles.title}>
                  {userInfo.name} {userInfo.lastname}
                </Title>
                <Caption style={styles.caption}>{user.email}</Caption>
              </View>
            </View>
          </View>

          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={({color, size}) => (
                <Icon name="home" size={24} color="#000" />
              )}
              label="Dashboard"
              onPress={() => {
                props.navigation.navigate('Home');
              }}
            />
            <DrawerItem
              icon={({color, size}) => (
                <Icon name="user" size={24} color="#000" />
              )}
              label="Profil"
              onPress={() => {
                props.navigation.navigate('Profile');
              }}
            />
            <DrawerItem
              icon={({color, size}) => (
                <Icon name="history" size={24} color="#000" />
              )}
              label="Istoric"
              onPress={() => {
                props.navigation.navigate('BookmarkScreen');
              }}
            />
            <DrawerItem
              icon={({color, size}) => (
                <Icon name="comment-dots" size={24} color="#000" />
              )}
              label="Recenzii"
              onPress={() => {
                props.navigation.navigate('SettingsScreen');
              }}
            />
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({color, size}) => (
            <Icon name="sign-out-alt" size={24} color="#000" />
          )}
          label="Deconectare"
          onPress={() => dispatch({type: 'LOGOUT_REQUEST'})}
        />
      </Drawer.Section>
    </View>
  );
}
