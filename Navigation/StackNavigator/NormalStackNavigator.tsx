import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../../Containers/SplashScreen/Splash.Screen';
import LoginScreen from '../../Containers/LoginScreen/Login.Screen';
import RegisterScreen from '../../Containers/RegisterScreen/Register.Screen';
import PasswordResetScreen from '../../Containers/PasswordResetScreen/PasswordReset.Screen';
import DashboardScreen from '../../Containers/DashboardScreen/Dashboard.Screen';
import {TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export type StackParamList = {
  LoginScreen: undefined;
  RegisterScreen: undefined;
  PasswordResetScreen: undefined;
  DashboardScreen: undefined;
  SplashScreen: undefined;
};

const Stack = createStackNavigator<StackParamList>();

const NormalStackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="SplashScreen" headerMode={'screen'}>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{
          gestureEnabled: false,
          gestureDirection: 'horizontal',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Autentificare"
        component={LoginScreen}
        options={{
          gestureEnabled: false,
          gestureDirection: 'horizontal',
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Înregistrare"
        component={RegisterScreen}
        options={{gestureEnabled: true, gestureDirection: 'horizontal'}}
        options={{
          title: 'Înregistrare',
          headerStyle: {
            backgroundColor: '#f4f4f6',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="Resetare parola"
        component={PasswordResetScreen}
        options={{
          title: 'Resetare parola',
          headerStyle: {
            backgroundColor: '#f4f4f6',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
      />
      <Stack.Screen
        name="Dashboard"
        component={DashboardScreen}
        options={({navigation}) => ({
          gestureEnabled: true,
          gestureDirection: 'horizontal',
          headerLeft: () => (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <View style={{paddingLeft: 15}}>
                <Icon name="bars" size={24} color="#000" />
              </View>
            </TouchableOpacity>
          ),
          headerRight: null,
        })}
      />
    </Stack.Navigator>
  );
};

export {NormalStackNavigator};
