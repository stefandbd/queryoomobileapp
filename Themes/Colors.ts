const colors = {
  primary: '#8f97f4',
  primaryDark: '#6973ec',
  white: '#ffffff',
  black: '#000000',
  charcoalGrey: '#30333a',
  boldGrey: '#101011',
  grey: '#202022',
  bgRoot: '#f4f4f6',
  red: '#ee3b3b',
  border: 'rgba(0, 0, 0, 0.1)',
  transparent: 'transparent',
};

export default colors;
