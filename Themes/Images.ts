const images = {
  logo: require('../Images/logo.png'),
  logoWhite: require('../Images/logoWhite.png'),
  loading: require('../Images/loading.gif'),
};

export default images;
