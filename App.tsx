import React, {Component} from 'react';
import createSagaMiddleware from 'redux-saga';
import AsyncStorage from '@react-native-community/async-storage';
import {applyMiddleware, createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import rootReducer from './Redux/reducers';
import rootSaga from './Sagas/sagas';
import DrawerNavigator from "./Navigation/DrawerNavigator/DrawerNavigator.Screen";
import 'react-native-gesture-handler';
import {composeWithDevTools} from 'redux-devtools-extension';
export const navigationRef = React.createRef();

const sagaMiddleware = createSagaMiddleware();
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth'],
};

export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default class App extends Component {
  handleNavigationChange = (
    prevState: NavigationState,
    newState: NavigationState,
    action: NavigationAction,
  ) => {
  };
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <DrawerNavigator />
        </PersistGate>
      </Provider>
    );
  }
}
