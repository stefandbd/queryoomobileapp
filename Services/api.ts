import apisauce from 'apisauce';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export const api = apisauce.create({
  baseURL: 'https://api.github.com/',
  headers: {Accept: 'application/vnd.github.v3+json'},
  timeout: 15000,
});

export const getProfile = (body: any) => {
  return api.get(`users/${body.username}`);
};

export const getFollower = (body: any) => {
  return api.get(`users/${body.username}/followers`);
};

export const resetPass = (username: string) => {
  console.log('resetPass service', username);
  return () => {
    auth()
      .sendPasswordResetEmail(username)
      .then(() => {
        return true;
      });
  };
};
