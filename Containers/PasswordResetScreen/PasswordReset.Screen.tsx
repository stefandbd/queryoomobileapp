import React, {useState} from 'react';
import {Image, View} from 'react-native';
import styles from './PasswordReset.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import images from '../../Themes/Images';
import GeneralButton from '../../Components/GeneralButton';
import {TextInput} from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import {RouteProp} from '@react-navigation/native';

interface Props {
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
  route: RouteProp<StackParamList, 'PasswordResetScreen'>;
}
interface State {
  username: any;
}

export default function PasswordResetScreen({navigation}, props) {
  const [username, setUsername] = useState(null);

  const onChangeEmail = (text: any) => {
    setUsername(text);
  };

  const onPasswordReset = () => {
    auth()
      .sendPasswordResetEmail(username)
      .then(function (user) {
        alert('Please check your email...');
      })
      .catch(function (e) {
        console.log(e);
      });
    navigation.navigate('Autentificare');
  };

  return (
    <View style={styles.mainContainer}>
      <Image style={styles.logo} source={images.logo} />
      <TextInput
        label="Adresă de email"
        mode="outlined"
        autoCapitalize="none"
        style={styles.loginInputView}
        value={username}
        onChangeText={(text) => onChangeEmail(text)}
      />
      <GeneralButton
        styleBtn={styles.btnLogin}
        styleText={styles.textGetData}
        title={'Restare parola'}
        onPress={onPasswordReset}
      />
    </View>
  );
}
