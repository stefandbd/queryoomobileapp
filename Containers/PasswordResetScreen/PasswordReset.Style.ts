import {StyleSheet} from 'react-native';
import {fontFamily, fontSize} from '../../Utils/const';
import ApplicationStyle from '../../Themes/Application.Style';
import colors from '../../Themes/Colors';
import Sizes from '../../Themes/Sizes';
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
  ...ApplicationStyle,
  mainContainer: {
    padding: 20,
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'flex-start',
  },
  btnLogin: {
    backgroundColor: colors.primary,
    width: Sizes.screen.width - 40,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 5,
    alignSelf: 'center',
  },
  textGetData: {
    fontFamily: fontFamily.light,
    color: colors.white,
    fontSize: 16,
    textTransform: 'uppercase',
    letterSpacing: 2
  },
  loginInputView: {
    fontFamily: fontFamily.regular,
    color: colors.white,
    fontSize: fontSize.medium,
    marginBottom: 15,
  },
  logo: {
    width: null,
    height: 200,
  },
});
