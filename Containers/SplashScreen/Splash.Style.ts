import {StyleSheet} from 'react-native';
import Sizes from '../../Themes/Sizes';

export default StyleSheet.create({
  mainContainer: {
    padding: 20,
    backgroundColor: '#fff',
    flex: 1,
    justifyContent: 'center',
  },
  logo: {
    width: 200,
    height: 300,
    position: 'absolute',
    top: Sizes.screen.height/2 - 150,
    right: Sizes.screen.width/2 - 100,
    zIndex: 999,
  },
  loading: {
    width: null,
    height: 200,
    zIndex: 111,
  },
});
