import React, {useEffect} from 'react';
import {Image, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './Splash.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import images from '../../Themes/Images';
import {loginRequest} from '../../Redux/Actions/Auth.Action';
import {RouteProp} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';

interface Props {
  loginRequest: (username: String, password: String) => {};
  registerRequest: (username: String, password: String) => {};
  user: {err: String; fetching: String};
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
  route: RouteProp<StackParamList, 'SplashScreen'>;
}

interface State {
  username: any;
  password: any;
}

export default function SplashScreen({navigation}, props) {
  const user = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const actionLoginRequest = (username: String, password: String) =>
    dispatch(loginRequest(username, password));

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    auth().onAuthStateChanged(function (theUser) {
      if (theUser) {
        const credential = auth.EmailAuthProvider.credential(
          theUser.email,
          user.data.moreInfo.password,
        );
        theUser
          .reauthenticateWithCredential(credential)
          .then(function () {
            console.warn('User re-authenticated.');
            actionLoginRequest(theUser.email, user.data.moreInfo.password);
          })
          .catch(function (error) {
            console.warn('user not authenticated', error);
          });
      } else {
        setTimeout(() => {
          navigation.navigate('Autentificare');
        }, 2000);
      }
    });
  }, []);

  return (
    <View style={styles.mainContainer}>
      <Image style={styles.logo} source={images.logo} />
      <Image style={styles.loading} source={images.loading} />
    </View>
  );
}
