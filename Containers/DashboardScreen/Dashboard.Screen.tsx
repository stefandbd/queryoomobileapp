import React, {useState} from 'react';
import {Text, Image, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './Dashboard.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import {logoutRequest} from '../../Redux/Actions/Auth.Action';
import images from '../../Themes/Images';
import GeneralButton from '../../Components/GeneralButton';
import {RouteProp} from '@react-navigation/native';

interface Props {
  user: {data: {moreInfo: {lastname: String; name: String}}};
  logoutRequest: () => {};
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
  route: RouteProp<StackParamList, 'DashboardScreen'>;
}

interface State {}

export default function DashboardScreen({navigation}, props) {
  const user = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const actionLogoutRequest = () => dispatch(logoutRequest());

  const onLogout = () => {
    actionLogoutRequest();
  };

  const {lastname, name} = user.data.moreInfo;
  return (
    <View style={styles.mainContainer}>
      <Image style={styles.logo} source={images.logo} />
      <Text>
        Salut, {name} {lastname}
      </Text>
      <GeneralButton
        styleBtn={styles.btnLogin}
        styleText={styles.textGetData}
        title={'Delogare'}
        onPress={onLogout}
      />
    </View>
  );
}
