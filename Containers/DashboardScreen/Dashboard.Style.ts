import {StyleSheet} from 'react-native';
import {fontFamily, fontSize} from '../../Utils/const';
import ApplicationStyle from '../../Themes/Application.Style';
import colors from '../../Themes/Colors';
import Sizes from '../../Themes/Sizes';
import { color } from 'react-native-reanimated';

export default StyleSheet.create({
  ...ApplicationStyle,
  // imgBg: {
  //   flex: 1,
  //   width: Sizes.screen.width,
  //   height: Sizes.screen.height,
  // },
  mainContainer: {
    padding: 20,
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
  },
  btnLogin: {
    backgroundColor: colors.primary,
    width: Sizes.screen.width - 40,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    borderRadius: 5,
    alignSelf: 'center',
  },
  textGetData: {
    fontFamily: fontFamily.light,
    color: colors.white,
    fontSize: 16,
    textTransform: 'uppercase',
    letterSpacing: 2
  },
  // loginInput: {
  //   backgroundColor: colors.transparent,
  //   width: Sizes.screen.widthTwoThirds,
  //   borderWidth: 0.5,
  //   borderColor: colors.black,
  //   height: 40,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   marginTop: 20,
  //   borderRadius: 5,
  //   alignSelf: 'center',
  //   padding: 10,
  // },
  loginInputView: {
    fontFamily: fontFamily.regular,
    color: colors.white,
    fontSize: fontSize.medium,
    marginBottom: 15,
  },
  registerContainer: {
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
  },
  noAccountText: {
    fontFamily: fontFamily.regular,
    color: colors.primary,
    fontSize: 16,
  },
  registerText: {
    fontFamily: fontFamily.bold,
    color: colors.primary,
    fontSize: 16,
    paddingLeft: 6,
  },
  logo: {
    width: null,
    height: 200,
  },
});
