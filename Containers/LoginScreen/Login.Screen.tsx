import React, {useState} from 'react';
import {Text, TouchableOpacity, Image, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './Login.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import images from '../../Themes/Images';
import GeneralButton from '../../Components/GeneralButton';
import {TextInput} from 'react-native-paper';
import {loginRequest} from '../../Redux/Actions/Auth.Action';
import LoadingView from '../../Components/LoadingView';
import {RouteProp} from '@react-navigation/native';

interface Props {
  loginRequest: (username: String, password: String) => {};
  registerRequest: (username: String, password: String) => {};
  user: {err: String; fetching: String};
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
  route: RouteProp<StackParamList, 'LoginScreen'>;
}

interface State {
  username: any;
  password: any;
}

export default function LoginScreen({navigation}, props) {
  const user = useSelector((state) => state.auth);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const actionLoginRequest = (username: String, password: String) =>
    dispatch(loginRequest(username, password));

  const onChangeEmail = (text: any) => {
    setUsername(text);
  };

  const onChangePass = (text: any) => {
    setPassword(text);
  };

  const onLogin = () => {
    actionLoginRequest(username, password);
    if (!user.fetching) {
      setUsername('');
      setPassword('');
    }
  };

  const goToRegister = () => {
    navigation.navigate('Înregistrare');
  };

  const goToResetPass = () => {
    navigation.navigate('Resetare parola');
  };

  if (user.fetching) {
    return <LoadingView />;
  } else {
    return (
      <View style={styles.mainContainer}>
        <Image style={styles.logo} source={images.logo} />
        <TextInput
          label="Adresă de email"
          mode="outlined"
          autoCapitalize="none"
          style={styles.loginInputView}
          value={username}
          onChangeText={(text) => onChangeEmail(text)}
        />
        <TextInput
          label="Parolă"
          mode="outlined"
          autoCapitalize="none"
          secureTextEntry={true}
          style={styles.loginInputView}
          value={password}
          onChangeText={(text) => onChangePass(text)}
        />
        {user.err ? (
          <View>
            <Text style={styles.errorText}>
              Parolă sau adresa de email introduse gresit.
            </Text>
            <Text style={styles.errorText}>Vă rugăm să reîncercați.</Text>
          </View>
        ) : null}
        <GeneralButton
          styleBtn={styles.btnLogin}
          styleText={styles.textGetData}
          title={'Autentificare'}
          onPress={onLogin}
        />
        <View style={styles.registerContainer}>
          <Text style={styles.noAccountText}>Nu ai cont?</Text>
          <TouchableOpacity onPress={goToRegister}>
            <Text style={styles.registerText}>Înregistrează-te</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.registerContainer}>
          <Text style={styles.noAccountText}>Ai uitat parola?</Text>
          <TouchableOpacity onPress={goToResetPass}>
            <Text style={styles.registerText}>Resetează</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
