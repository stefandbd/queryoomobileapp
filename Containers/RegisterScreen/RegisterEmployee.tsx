import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import styles from './Register.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import GeneralButton from '../../Components/GeneralButton';
import {TextInput} from 'react-native-paper';
import {registerRequestEmployee} from '../../Redux/Actions/Auth.Action';

interface Props {
  registerRequest: (email: String, password: String) => {};
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
}

interface State {}

export default function RegisterEmployee({navigation}, props) {
  const [username, setUsername] = useState(null);
  const [usernameValid, setUsernameValid] = useState(null);
  const [password, setPassword] = useState(null);
  const [name, setName] = useState(null);
  const [rPassword, setRPassword] = useState(null);
  const [nameValid, setNameValid] = useState(null);
  const [lastname, setLastName] = useState(null);
  const [lastnameValid, setLastNameValid] = useState(null);
  const [passwordsMatch, setPasswordMatch] = useState(null);

  const dispatch = useDispatch();
  const actionRegisterRequestEmployee = (
    username: String,
    password: String,
    name: String,
    lastname: String,
    userType: String,
  ) =>
    dispatch(
      registerRequestEmployee(username, password, name, lastname, userType),
    );

  const onChangeEmail = (text: any) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      setUsername(text);
      setUsernameValid(false);
      return false;
    } else {
      setUsername(text);
      setUsernameValid(true);
    }
  };

  const onChangePass = (text: any) => {
    setPassword(text);
  };

  const onChangeRpass = (text: any) => {
    setRPassword(text);
  };

  const onChangeName = (text: any) => {
    if (text.length > 2) {
      setName(text);
      setNameValid(true);
    } else if (text.length <= 2) {
      setName(text);
      setNameValid(false);
    }
  };

  const onChangeLastName = (text: any) => {
    if (text.length > 2) {
      setLastName(text);
      setLastNameValid(true);
    } else if (text.length <= 2) {
      setLastName(text);
      setLastNameValid(false);
    }
  };

  const onSignUp = () => {
    if (password !== rPassword) {
      setPasswordMatch(false);
    } else if (usernameValid && nameValid && lastnameValid) {
      setPasswordMatch(true);
      actionRegisterRequestEmployee(
        username,
        password,
        name,
        lastname,
        'users-employee',
      );
    }
  };

  return (
    <View style={styles.mainContainerTab}>
      <TextInput
        label="Adresă de email"
        mode="outlined"
        autoCapitalize="none"
        style={styles.loginInputView}
        value={username}
        onChangeText={(text) => onChangeEmail(text)}
      />
      {usernameValid || usernameValid === null || username === '' ? null : (
        <Text style={styles.errorText}>Adresa de email nu este corectă</Text>
      )}
      <TextInput
        label="Parolă"
        mode="outlined"
        autoCapitalize="none"
        secureTextEntry={true}
        style={styles.loginInputView}
        value={password}
        onChangeText={(text) => onChangePass(text)}
      />
      {passwordsMatch || passwordsMatch === null || password === '' ? null : (
        <Text style={styles.errorText}>Parolele nu sunt identice.</Text>
      )}
      <TextInput
        label="Repetă Parola"
        mode="outlined"
        autoCapitalize="none"
        secureTextEntry={true}
        style={styles.loginInputView}
        value={rPassword}
        onChangeText={(text) => onChangeRpass(text)}
      />
      {passwordsMatch || passwordsMatch === null || rPassword === '' ? null : (
        <Text style={styles.errorText}>Parolele nu sunt identice.</Text>
      )}
      <TextInput
        label="Nume"
        mode="outlined"
        style={styles.loginInputView}
        value={name}
        onChangeText={(text) => onChangeName(text)}
      />
      {nameValid || nameValid === null || name === '' ? null : (
        <Text style={styles.errorText}>Numele nu este corect</Text>
      )}
      <TextInput
        label="Prenume"
        mode="outlined"
        style={styles.loginInputView}
        value={lastname}
        onChangeText={(text) => onChangeLastName(text)}
      />
      {lastnameValid === null || lastname === '' || lastnameValid ? null : (
        <Text style={styles.errorText}>Prenumele nu este corect</Text>
      )}
      <GeneralButton
        styleBtn={styles.btnLogin}
        styleText={styles.textGetData}
        title={'Înregistrare'}
        onPress={onSignUp}
      />
    </View>
  );
}
