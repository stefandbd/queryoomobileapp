import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import styles from './Register.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import GeneralButton from '../../Components/GeneralButton';
import {TextInput} from 'react-native-paper';
import {registerRequestEmployer} from '../../Redux/Actions/Auth.Action';

interface Props {
  registerRequest: (email: String, password: String) => {};
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
}

interface State {}

export default function RegisterEmployer({navigation}, props) {
  const [username, setUsername] = useState(null);
  const [usernameValid, setUsernameValid] = useState(null);
  const [password, setPassword] = useState(null);
  const [company, setCompany] = useState(null);
  const [rPassword, setRPassword] = useState(null);
  const [companyValid, setCompanyValid] = useState(null);
  const [cui, setCui] = useState(null);
  const [cuiValid, setCuiValid] = useState(null);
  const [passwordsMatch, setPasswordMatch] = useState(null);

  const dispatch = useDispatch();
  const actionRegisterRequestEmployer = (
    username: String,
    password: String,
    company: String,
    cui: String,
    userType: String,
  ) =>
    dispatch(
      registerRequestEmployer(username, password, company, cui, userType),
    );
  const onChangeEmail = (text: any) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      setUsername(text);
      setUsernameValid(false);
      return false;
    } else {
      setUsername(text);
      setUsernameValid(true);
    }
  };

  const onChangePass = (text: any) => {
    setPassword(text);
  };

  const onChangeRpass = (text: any) => {
    setRPassword(text);
  };
  const onChangeCompany = (text: any) => {
    if (text.length > 2) {
      setCompany(text);
      setCompanyValid(true);
    } else if (text.length <= 2) {
      setCompany(text);
      setCompanyValid(false);
    }
  };

  const onChangeCUI = (text: any) => {
    String.prototype.isNumber = function () {
      return /^\d+$/.test(this);
    };
    if (text.length > 8 || text.length < 8 || !text.isNumber()) {
      setCui(text);
      setCuiValid(false);
    } else if (text.length === 8 && text.isNumber()) {
      setCui(text);
      setCuiValid(true);
    }
  };

  const onSignUp = () => {
    if (password !== rPassword) {
      setPasswordMatch(false);
    } else if (usernameValid && companyValid && cuiValid) {
      setPasswordMatch(true);
      actionRegisterRequestEmployer(
        username,
        password,
        company,
        cui,
        'users-employer',
      );
    }
  };

  return (
    <View style={styles.mainContainerTab}>
      <TextInput
        label="Adresă de email"
        mode="outlined"
        autoCapitalize="none"
        style={styles.loginInputView}
        value={username}
        onChangeText={(text) => onChangeEmail(text)}
      />
      {usernameValid || usernameValid === null || username === '' ? null : (
        <Text style={styles.errorText}>Adresa de email nu este corectă</Text>
      )}
      <TextInput
        label="Parolă"
        mode="outlined"
        autoCapitalize="none"
        secureTextEntry={true}
        style={styles.loginInputView}
        value={password}
        onChangeText={(text) => onChangePass(text)}
      />
      {passwordsMatch || passwordsMatch === null || password === '' ? null : (
        <Text style={styles.errorText}>Parolele nu sunt identice.</Text>
      )}
      <TextInput
        label="Repetă Parola"
        mode="outlined"
        autoCapitalize="none"
        secureTextEntry={true}
        style={styles.loginInputView}
        value={rPassword}
        onChangeText={(text) => onChangeRpass(text)}
      />
      {passwordsMatch || passwordsMatch === null || rPassword === '' ? null : (
        <Text style={styles.errorText}>Parolele nu sunt identice.</Text>
      )}
      <TextInput
        label="Companie"
        mode="outlined"
        style={styles.loginInputView}
        value={company}
        onChangeText={(text) => onChangeCompany(text)}
      />
      {companyValid || companyValid === null || company === '' ? null : (
        <Text style={styles.errorText}>Compania nu este corectă</Text>
      )}
      <TextInput
        label="CUI"
        mode="outlined"
        style={styles.loginInputView}
        value={cui}
        onChangeText={(text) => onChangeCUI(text)}
      />
      {cuiValid || cuiValid === null || cui === '' ? null : (
        <Text style={styles.errorText}>CUI-ul nu este corect</Text>
      )}
      <GeneralButton
        styleBtn={styles.btnLogin}
        styleText={styles.textGetData}
        title={'Înregistrare'}
        onPress={onSignUp}
      />
    </View>
  );
}
