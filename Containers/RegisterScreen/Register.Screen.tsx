import React, {useState} from 'react';
import {Dimensions, View, Text} from 'react-native';
import styles from './Register.Style';
import {StackNavigationProp} from '@react-navigation/stack';
import {StackParamList} from '../../Navigation/StackNavigator/NormalStackNavigator';
import {CompositeNavigationProp} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {DrawerParamList} from '../../Navigation/DrawerNavigator/DrawerNavigator.Screen';
import RegisterEmployee from './RegisterEmployee';
import RegisterEmployer from './RegisterEmployer';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {RouteProp} from '@react-navigation/native';

const FirstRoute = () => <RegisterEmployee />;
const SecondRoute = () => <RegisterEmployer />;

interface Props {
  registerRequest: (username: String, password: String) => {};
  navigation: CompositeNavigationProp<
    StackNavigationProp<StackParamList>,
    DrawerNavigationProp<DrawerParamList>
  >;
  route: RouteProp<StackParamList, 'RegisterScreen'>;
}

interface State {
  index: Number;
  routes: Array<Object>;
}

export default function RegisterScreen({navigation}, props) {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: 'Angajat'},
    {key: 'second', title: 'Angajator'},
  ]);
  // const combination = {index, routes};

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });


  const renderTabBar = (props: any) => (
    <TabBar
      {...props}
      renderLabel={({route, focused}) => (
        <View
          style={{
            backgroundColor: focused ? '#8f97f4' : 'transparent',
            height: 45,
            width: Dimensions.get('window').width / 3,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 16,
            borderWidth: focused ? 0 : 0.5,
            borderColor: '#8f97f4',
          }}>
          <Text
            style={{
              color: focused ? 'white' : '#8f97f4',
              margin: 8,
              fontSize: 14,
              textTransform: 'uppercase',
              letterSpacing: 1,
            }}>
            {route.title}
          </Text>
        </View>
      )}
      scrollEnabled
      indicatorStyle={styles.indicatorStyle}
      style={styles.indicatorContainerStyle}
      tabStyle={styles.tabBarStyle}
    />
  );
  return (
    <View style={styles.mainContainer}>
      <TabView
        renderTabBar={renderTabBar}
        renderScene={renderScene}
        navigationState={{ index, routes }}
        onIndexChange={setIndex}
        initialLayout={{width: Dimensions.get('window').width}}
      />
    </View>
  );
}
