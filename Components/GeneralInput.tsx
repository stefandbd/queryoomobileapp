import React, {FC} from 'react';
import {StyleProp, View, TextInput, ViewStyle} from 'react-native';

interface Props {
  onChangeText: () => any;
  styleInput?: StyleProp<ViewStyle>;
  styleView?: StyleProp<ViewStyle>;
  placeholderText: String;
}

const GeneralInput: FC<Props> = ({
  styleInput,
  styleView,
  onChangeText,
  placeholderText,
}: Props) => {
  return (
    <View style={styleView}>
      <TextInput
        placeholderTextColor="#090a40"
        style={styleInput}
        placeholder={placeholderText}
        autoCapitalize={'none'}
        onChangeText={onChangeText}
      />
    </View>
  );
};

export default GeneralInput;
