import {call, put, takeLatest} from 'redux-saga/effects';
import {
  UPDATE_AVATAR_REQUEST,
  updateAvatarError,
  updateAvatarSuccess,
} from '../Redux/Actions/Profile.Action';
import {Action} from '../Redux/reducers';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

export function* watchUpdateAvatar() {
  yield takeLatest(UPDATE_AVATAR_REQUEST, handleUpdateAvatar);
}

export function* syncFileUrl(action: Action) {
  try {
    const {reference, source} = action.payload;
    const authh = auth();
    const fileName = source.uri.substring(source.uri.lastIndexOf('/') + 1);
    const url = yield call(
      reference._storage._nativeModule.getDownloadURL,
      'gs://queryoo-32c95.appspot.com/avatars/' +
        authh._user.uid +
        '/' +
        fileName,
    );
    firestore().collection('users').doc(authh._user.uid).update({
      photoURL: url,
    });
    yield put(updateAvatarSuccess(url));
  } catch (error) {
    yield put(updateAvatarError(error));
  }
}

function* handleUpdateAvatar(action: Action) {
  try {
    const {reference, source} = action.payload;
    const task = reference.putFile(source.uri);
    task.on('state_changed', (snapshot) => {
      const pct = (snapshot.bytesTransferred * 100) / snapshot.totalBytes;
      console.warn(`${pct}%`);
    });
    yield task;
    yield call(syncFileUrl, action);
  } catch (error) {
    yield put(updateAvatarError(error));
  }
}
