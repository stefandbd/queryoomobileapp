import {all, call, takeEvery} from 'redux-saga/effects';
import {watchUpdateAvatar, syncFileUrl} from './Profile.Saga';
import {watchGetFollower} from './Follower.Saga';
import {watchRegister, watchLogin, watchLogout, watchResetPass} from './Auth.Sagas';

export default function* rootSaga() {
  yield all([watchUpdateAvatar(), watchGetFollower(), watchRegister(), watchLogin(), watchLogout(), watchResetPass()]);
  yield call(syncFileUrl);
}
