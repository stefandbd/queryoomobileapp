import {call, put, takeLatest} from 'redux-saga/effects';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {
  registerSuccess,
  registerError,
  REGISTER_REQUEST,
  LOGIN_REQUEST,
  loginSuccess,
  loginError,
  LOGOUT_REQUEST,
  logoutError,
  logoutSuccess,
  RESET_PASSWORD_REQUEST,
  passResetError,
  passResetSuccess,
} from '../Redux/Actions/Auth.Action';
import {Action} from '../Redux/reducers';
import * as RootNavigation from '../App';
import RNSmtpMailer from 'react-native-smtp-mailer';

export function* watchRegister() {
  yield takeLatest(REGISTER_REQUEST, handleRegister);
}

function* handleRegister(action: Action) {
  try {
    const {username, password, type, name, lastname, company} = action.payload;
    const authh = auth();
    const response = yield call(
      [authh, authh.createUserWithEmailAndPassword],
      username,
      password,
    );
    // response.user.updateProfile({
    //   displayName: type,
    // });
    if (type === 'users-employee') {
      firestore()
        .collection('users')
        .doc(response.user.uid)
        .set({
          name: name,
          lastname: lastname,
          userType: 'employee',
        })
        .then(() => {
          console.warn('User added!');
        });
    } else {
      firestore()
        .collection('users')
        .doc(response.user.uid)
        .set({
          company: company,
          userType: 'employer',
        })
        .then(() => {
          console.warn('User added!');
        });
    }

    yield put(registerSuccess(response));
    RNSmtpMailer.sendMail({
      mailhost: 'devnative.ro',
      port: '465',
      ssl: true,
      username: 'office@devnative.ro',
      password: '4xB7TVv8uOMA',
      from: 'office@devnative.ro',
      recipients: `${username}`,
      subject: 'Bine ai venit la Queryoo!',
      htmlBody:
        '<h1>Bine ai venit la Queryoo!</h1><p>Contul tau a fost creat cu succes si este activ.</p><h3>Detalii cont</h3><ul><li>Utilizator: ' +
        username +
        '</li></ul><span>Multumim,</span><br><span>Echipa <strong>Queryoo</strong></span>',
      attachmentPaths: [],
      attachmentNames: [], //only used in android, these are renames of original files. in ios filenames will be same as specified in path. In ios-only application, leave it empty: attachmentNames:[]
      attachmentTypes: [], //needed for android, in ios-only application, leave it empty: attachmentTypes:[]. Generally every img(either jpg, png, jpeg or whatever) file should have "img", and every other file should have its corresponding type.
    })
      .then((success) => console.warn(success))
      .catch((err) => console.warn(err));
    RootNavigation.navigate('Autentificare', {});
    // if to redirect to login saga after registering ,
    // and instead of .collection(response.user.displayName), use .collection(type)
    // yield call(handleLogin, action);
  } catch (error) {
    yield put(registerError(error));
  }
}

export function* watchLogin() {
  yield takeLatest(LOGIN_REQUEST, handleLogin);
}

function* handleLogin(action: Action) {
  try {
    const {username, password} = action.payload;
    const authh = auth();
    const response = yield call(
      [authh, authh.signInWithEmailAndPassword],
      username,
      password,
    );
    const collection = firestore().collection('users').doc(response.user.uid);
    const querySnap = yield call([collection, collection.get]);
    querySnap._data.password = password;
    const newObj = {
      user: response.user,
      moreInfo: querySnap._data,
    };
    yield put(loginSuccess(newObj));
    RootNavigation.navigate('Dashboard', {});
  } catch (error) {
    yield put(loginError(error));
  }
}

export function* watchLogout() {
  yield takeLatest(LOGOUT_REQUEST, handleLogout);
}

function* handleLogout(action: Action) {
  console.log('sagas ---');
  try {
    const authh = auth();
    yield call([authh, authh.signOut]);
    yield put(logoutSuccess());
    RootNavigation.navigate('Autentificare', {});
  } catch (error) {
    yield put(logoutError(error));
  }
}

export function* watchResetPass() {
  yield takeLatest(RESET_PASSWORD_REQUEST, handleResetPass);
}

function* handleResetPass(action: Action) {
  const {username} = action.payload;
  try {
    const authh = auth();
    const response = authh.sendPasswordResetEmail(username);
    yield put(passResetSuccess(response));
    RootNavigation.navigate('Autentificare', {});
  } catch (error) {
    yield put(passResetError(error));
  }
}
